import java.util.*;

public class TwoSum{
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        System.out.println("Nhap so k: ");
        int k = in.nextInt();
		System.out.println("Nhap so N: ");
		int N = in.nextInt();
		
		int []A = new int[k];
		Random rd = new Random();
		for (int x=0; x<k; x++){
			boolean ans = true;
				do {
					int M = rd.nextInt(2*N)-N;
					ans = true;
					for (int y=0; y<x; y++){
						if (A[y] == M){
							ans = false;
							y = x;
						}
					}
					if (ans == true){
						A[x] = M;
						System.out.print(A[x] + "   ");
					}
				}
			while (ans == false);
		}
		
		System.out.println();
		System.out.println("Cac cap so thoa man dieu kien la: ");
		for (int x=0; x<k; x++){
			for (int y=x+1; y<k; y++)
				if (A[x] + A[y] == 0) 
					System.out.println(A[x] + "      " + A[y]);
		}
	}
}
