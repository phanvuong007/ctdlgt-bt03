import java.util.*;

public class Names{
	public static void main(String []args){
		String []ho = {"Ly","Tran","Le","Nguyen","Vo","Phan","Pham","Trinh"};
		String []dem = {"Van", "Thi", "Minh", ""};
		String []ten = {"Dung","An","Ha","Bao","Thanh","Hoang","Vuong","Thao","Thang"};
		
		Scanner in = new Scanner(System.in);
		System.out.print("Nhap N: ");
		int N = in.nextInt();
		Random rd = new Random();
		
		String []hoten = new String[N];
		
		System.out.println("Cac ten duoc sinh ra la: ");
		for (int x = 0; x<N; x++){
			int h = rd.nextInt(ho.length);
			int d = rd.nextInt(dem.length);
			int t = rd.nextInt(ten.length);
			hoten[x] = ho[h] + " " + dem[d] + " " + ten[t];
			System.out.println(hoten[x]);
		}
		
		System.out.println();
		System.out.println("Cac ten sau khi sap xep: ");
		
		Arrays.sort(hoten);
		for (int x=0; x<N; x++)
			System.out.println(hoten[x]);
		
	}
}