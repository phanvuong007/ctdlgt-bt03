import java.util.*;

public class OneSum{
	public static void main(String [] args){
			System.out.print("Nhap so N: ");
			Scanner in = new Scanner(System.in);
			int N = in.nextInt();
			
			int []A;
			A = new int[N];
			
			boolean ans = false;
			
			for (int x=0; x<N; x++){
				Random rd = new Random(); 
				A[x] = rd.nextInt(100);
				System.out.print(A[x] + "   ");
				if (A[x] == 0){
					ans = true;
				}
			}
			
			System.out.println();
			if (ans == true)
				System.out.print("Mang co so 0");
			else
				System.out.print("Mang khong co so 0");
				
	}
}